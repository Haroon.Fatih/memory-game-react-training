import './App.css'
import { useEffect, useState } from 'react'
import SingleCard from './components/SingleCard'

// create an array of cards which does not change on each component renter
const cardImages = [
  { "src": "/img/helmet-1.png", matched: false },
  { "src": "/img/potion-1.png", matched: false },
  { "src": "/img/ring-1.png", matched: false },
  { "src": "/img/scroll-1.png", matched: false },
  { "src": "/img/shield-1.png", matched: false },
  { "src": "/img/sword-1.png", matched: false },
]

function App() {
  const [cards, setCards] = useState([])
  const [turns, setTurns] = useState(0)
  const [choiceOne, setChoiceOne] = useState(null)
  const [choiceTwo, setChoiceTwo] = useState(null)
  const [disabled, setDisabled] = useState(false)

  // function to shuffle cards 
  const shuffleCards = () => {
    // duplicate cards, 
    // the sort randomise order
    // randomise the id
    const shuffleCards = [...cardImages, ...cardImages]
      .sort(() => Math.random() - 0.5)
      .map((card) => ({ ...card, id: Math.random() }))

      setChoiceOne(null)
      setChoiceTwo(null)

      setCards(shuffleCards)
      setTurns(0)
  }

  // handle a choice
  const handleChoice = (card) => {
    choiceOne ? setChoiceTwo(card) : setChoiceOne(card)
  }

  // Compare 2 selected cards
  useEffect(() => {
    
    if (choiceOne && choiceTwo) {
      setDisabled(true)
      if (choiceOne.src === choiceTwo.src) {
        setCards(prevCards => {
          return prevCards.map(card => {
            if (card.src === choiceOne.src) {
              return {...card, matched: true}
            } else {
              return card
            }
          })
        })

        resetTurn()
      } else {

        setTimeout(() =>  resetTurn(), 1000)
      }

    }
  }, [choiceOne, choiceTwo])

  console.log(cards)

  // reset choices and increase turn count
  const resetTurn = () => {
    setChoiceOne(null)
    setChoiceTwo(null)
    setTurns(prevTurns => prevTurns + 1)
    setDisabled(false)
  }

  // Start game automatically

  useEffect(() => {
    shuffleCards()
  }, [])


  return (
    <div className="App">
      <h1>Magic Match</h1>
      <button onClick={shuffleCards}>New Game</button>

      <div className = "card-grid">
        {cards.map(card => (
          <SingleCard 
            key={card.id} 
            card = {card} 
            handleChoice = {handleChoice}
            flipped = {card === choiceOne || card === choiceTwo || card.matched }
            disabled = {disabled}
          />
        ))}
      </div>
      <p>Turns: {turns} </p>
    </div>
  );
}

export default App